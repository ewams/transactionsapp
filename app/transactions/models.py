from django.db import models


class PurchaseOrSale(models.TextChoices):
    PURCHASE = 'purchase'
    SALE = 'sale'


class CSVFile(models.Model):
    date = models.DateField(blank=False)
    purchase_or_sale = models.CharField(max_length=8, choices=PurchaseOrSale.choices, blank=False)
    country = models.CharField(max_length=2, blank=False)
    currency = models.CharField(max_length=3, blank=False)
    net = models.DecimalField(max_digits=12, decimal_places=2, blank=False)
    vat = models.DecimalField(max_digits=12, decimal_places=2, blank=False)


class CSVFileValidationErrors(models.Model):
    index = models.IntegerField()
    errors = models.JSONField()
