from rest_framework import serializers
from .country_data import COUNTRY_ALPHA_2_LIST, COUNTRY_CURRENCY_LIST


PURCHASE_OR_SALE_CHOICE = (
    ("purchase", "Purchase"),
    ("sale", "Sale"),
)


class CSVFileSerializer(serializers.Serializer):
    date = serializers.DateField()
    purchase_or_sale = serializers.ChoiceField(choices=PURCHASE_OR_SALE_CHOICE)
    country = serializers.CharField(max_length=2)
    currency = serializers.CharField(max_length=3)
    net = serializers.DecimalField(max_digits=12, decimal_places=2)
    vat = serializers.DecimalField(max_digits=12, decimal_places=2)

    def validate(self, data):
        if data['country'] not in COUNTRY_ALPHA_2_LIST:
            raise serializers.ValidationError("Invalid country specification")
        if data['currency'] not in COUNTRY_CURRENCY_LIST:
            raise serializers.ValidationError("Invalid currency specification")
        return data


class FileUploadSerializer(serializers.Serializer):
    file = serializers.FileField()

