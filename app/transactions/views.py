from .serializers import CSVFileSerializer, FileUploadSerializer
from .models import CSVFile, CSVFileValidationErrors
from .utils import format_csv_data, rename_header_columns, format_date, format_country
from rest_framework import viewsets, generics, status, mixins
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
import pandas as pd
from rest_framework.pagination import PageNumberPagination


class ProcessFileViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = CSVFile.objects.all()
    serializer_class = FileUploadSerializer
    parser_classes = [FileUploadParser]

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file = serializer.validated_data['file']
        csv_reader = pd.read_csv(file)
        rename_header_columns(csv_reader)

        for index, row in csv_reader.iterrows():
            row_data = format_csv_data(row)
            csv_file_serializer = CSVFileSerializer(data=row_data)
            if csv_file_serializer.is_valid():
                # Skip rows where the year isn't 2020
                if csv_file_serializer.validated_data["date"].year == 2020:
                    csv_file = CSVFile(**csv_file_serializer.validated_data)
                    csv_file.save()
            else:
                validation_errors = CSVFileValidationErrors(index=index, errors=csv_file_serializer.errors)
                validation_errors.save()

        return Response({"status": "success"}, status.HTTP_201_CREATED)


class RetrieveRowsViewSet(viewsets.ReadOnlyModelViewSet, generics.GenericAPIView):
    queryset = CSVFile.objects.all()
    serializer_class = CSVFileSerializer
    pagination_class = PageNumberPagination

    def list(self, request):
        country = request.query_params.get("country")
        date = request.query_params.get("date")
        # To do
        # currency = request.query_params.get("currency")
        page_size = request.query_params.get("page_size")
        queryset = CSVFile.objects.all().order_by('id')

        paginator = self.pagination_class()
        paginator.page_query_param = 'page'

        if page_size:
            paginator.page_size = page_size
        if country:
            query_country = format_country(country)
            queryset = queryset.filter(country=query_country).order_by('id')
        elif date:
            query_date = format_date(date)
            queryset = queryset.filter(date=query_date).order_by('id')

        paginated_queryset = paginator.paginate_queryset(queryset=queryset.order_by('id'), request=request)
        serializer = CSVFileSerializer(paginated_queryset, many=True)

        return paginator.get_paginated_response(serializer.data)




