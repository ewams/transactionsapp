from .country_data import COUNTRY_LIST, COUNTRY_ALPHA_2_LIST
from datetime import datetime

PURCHASE = 'purchase'
SALE = 'sale'


def format_amount(raw_amount):
    formatted_value = str(raw_amount)
    try:
        formatted_value = "{:.2f}".format(raw_amount)
    except Exception as e:
        print(e)
    return formatted_value


def format_currency(raw_currency):
    formatted_currency = str(raw_currency).strip()
    if len(formatted_currency) != 3:
        return ""
    return formatted_currency


def format_csv_data(raw_csv_data):
    dict = {
        "date": format_date(raw_csv_data["date"]),
        "purchase_or_sale": format_purchase_or_sale(raw_csv_data["purchase_or_sale"]),
        "country": format_country(raw_csv_data["country"]),
        "currency": format_currency(raw_csv_data["currency"]),
        "net": format_amount(raw_csv_data["net"]),
        "vat": format_amount(raw_csv_data["vat"])
    }

    return dict


def format_country(raw_country):
    country = str(raw_country).strip()
    if country:
        if not len(country) == 2:
            # Great Britain not in COUNTRY LIST. United Kingdom is instead.
            if country == "Great Britain":
                country = "GB"
            elif country in COUNTRY_LIST:
                index = COUNTRY_LIST.index(country)
                country = COUNTRY_ALPHA_2_LIST[index]
    return country


def format_date(raw_date):
    date = str(raw_date).strip()
    try:
        date = datetime.strptime(raw_date, "%Y/%m/%d").date()
    except Exception as e:
        print(e)
    return date


def format_purchase_or_sale(raw_purchase_or_sale):
    formatted_purchase_or_sale = str(raw_purchase_or_sale).strip()
    if PURCHASE in raw_purchase_or_sale.lower():
        formatted_purchase_or_sale = PURCHASE
    elif SALE in raw_purchase_or_sale.lower():
        formatted_purchase_or_sale = SALE
    return formatted_purchase_or_sale


def rename_header_columns(dataframe):
    dataframe.rename(
        columns={
            "Date": "date",
            "Purchase/Sale": "purchase_or_sale",
            "Currency": "currency",
            "Country": "country",
            "Net": "net",
            "VAT": "vat"
        },
        inplace=True
    )
