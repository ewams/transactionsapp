### Running the app
1. Activate a virtual environment.
2. Install the dependencies in requirements.txt with pip3.
3. Navigate to the `app` directory and execute `./manage.py migrate`.
4. With the virtual environment activated, launch the server by executing `./manage.py runserver`.

### Currency implementation

1. Implement cache with Django database caching.
2. Request will pass through the cache first and if there's no data, the ECB endpoint will be called.
3. The key and query parameters section will be used as the key for the cached data in the database 
excluding the format query parameter.

### Scalability
1. I would modify the database to improve its performance i.e. normalization, sharding, indexing,
having a read replica for frequently accessed data.
2. I would cache frequently accessed data with Redis for example.
3. I would have separate worker processes for uploading large transactions. A limitation here is
even though the frontend can be scaled out, scaling out the backend databases may not be as fast. In addition, provision would need to be made
to minimize the effect of noisy neighbours between clients i.e. when clients with smaller datasets 
end up waiting for long because of users with large datasets.
4. I would establish microservices and use a platform such as Kubernetes to scale-out and scale-in
resources as per the demand. A limitation here is network latency will have a greater effect on the end user's
experience.
5. I would use a database platform that supports clustering for easier load balancing, greater availability, and 
increased data redundancy. A limitation of using a clustered database is the fact that recovering from data corruption
can be complex.